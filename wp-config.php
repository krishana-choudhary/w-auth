<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'w_auth');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I|br|n?pHue}$ hp;cp6`{i`_.$Hj)qAPU{>o3GR)Qu<3%I^_[}l9Q:)>%.0G])b');
define('SECURE_AUTH_KEY',  'HTIeg{C`fRk9`Fm+J;+O?cE4bX;X}u=dPqRrG32_r>~1P4V8b5.hD_`^%5o4g[k&');
define('LOGGED_IN_KEY',    '97^ZYcv|q.{*sAT@o;(V@Ab I.Abk_!idHby58~>PjVMdob(M10Ftf2?)}z$h5}A');
define('NONCE_KEY',        'US&8!O-P=U@zU6bD)pkJ4{wpu^D(*i+1WG/:/)^)GPL$7U, b8B]omj;o|TDQOOh');
define('AUTH_SALT',        '}UEi1+*;sr9T86k2;&cI3%u7.`Un@v3_OpD;3p<.]>Rxn2)klqT$X0j|LrRp00Sc');
define('SECURE_AUTH_SALT', 'Vey%Ilj6@pZ?RGyFR=Sq=jPqP$<#y5L,akbHkdp~gANmqNGe&;Yj|j=nPzhO3:Xp');
define('LOGGED_IN_SALT',   '20nU|PR-+TVUC/qy?^&RnrZBy8}d(LGb)e:@{61nLOWM>Vt07dZxg?^)Kx&45BJ@');
define('NONCE_SALT',       'vIFzWtwv^JsI2>d6U1nIGN((o9ItIe.`REsX5Sd_A28xh{B[9QW[)m4tI!7:&n=@');
define('JWT_AUTH_SECRET_KEY', 'f7fR+zg,^5j%VedFgPQ7]a+O4ov!E,+-6of|e}$%iPS74HX6_4$,-JO0Jq?cg4 A');
define('JWT_AUTH_CORS_ENABLE', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
